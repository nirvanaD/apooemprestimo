package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Americano;
import model.Parcela;
import model.Price;
import model.Sac;
import model.ToPdf;

@ManagedBean
@SessionScoped
public class PrincipalMB implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private double montante;
	private double juros;
	private int qtParcelas;
	private Parcela parcela;
	private List<Parcela> parcelas;
	


	//2,3,4 = sac, price, americano
	private int tipo= 0;
	private boolean sac = false;
	private boolean price = false;
	private boolean americano = false;

	private Price calcprice;
	private Americano calcamericano;
	private Sac calcsac;
	
	public boolean isSac() {
		return sac;
	}

	public boolean isPrice() {
		return price;
	}

	public boolean isAmericano() {
		return americano;	
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
		this.sac = tipo == 2;
		this.price = tipo == 3;
		this.americano = tipo == 4;
	}

	

	public PrincipalMB() {
		super();
		this.parcelas = new ArrayList<Parcela>();
		this.montante = 0;
		this.juros = 0;
		this.qtParcelas = 0;
		
	}
	
	
	
	public List<Parcela> getParcelas() {
		if(this.isAmericano()){
			calcamericano = new Americano();
			this.parcelas = calcamericano.calculaAmericano(this.montante, this.qtParcelas, this.juros);			
			
		}
		else if(this.isPrice()){
			calcprice = new Price();
			this.parcelas = calcprice.calculaPrice(this.montante, this.qtParcelas, this.juros);
		}
		else if(this.isSac()){
			calcsac = new Sac();
			this.parcelas = calcsac.calculaSac(this.montante, this.qtParcelas, this.juros);
		}
		return this.parcelas;
	}
	

	public String gerarPlanilha(){
		return "planilha.jsf?faces-redirect=true";
	}
	
	public String gerarPdf(){
		ToPdf t = new ToPdf();
		t.gerarPdf(this.parcelas, this.qtParcelas, this.tipo);
		return "index.jsf?faces-redirect=true";
	}

	public double getMontante() {
		return montante;
	}

	public void setMontante(double montante) {
		this.montante = montante;
	}

	public double getJuros() {
		return juros;
	}

	public void setJuros(double juros) {
		this.juros = juros;
	}

	public int getQtParcelas() {
		return qtParcelas;
	}

	public void setQtParcelas(int qtParcelas) {
		this.qtParcelas = qtParcelas;
	}

	public Parcela getParcela() {
		return parcela;
	}

	public void setParcela(Parcela parcela) {
		this.parcela = parcela;
	}

	
	public void setParcelas(List<Parcela> parcelas) {
		this.parcelas = parcelas;
	}

	public void setSac(boolean sac) {
		this.sac = sac;
	}

	public void setPrice(boolean price) {
		this.price = price;
	}

	public void setAmericano(boolean americano) {
		this.americano = americano;
	}

	




}
