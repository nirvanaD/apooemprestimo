package model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;




import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



public class ToPdf {
	private static int plus = 22;
	private Document doc;
	private OutputStream os;
	private String titulo = "Previs�o de Empr�stimo";
	private String cabecalho;

	private int colunas = 5;
	private int numeroLinhas;
	private List<String> coluna1;
	private List<String> coluna2;
	private List<String> coluna3;
	private List<String> coluna4;
	private List<String> coluna5;

	//private String ultimaLinhaTotal = "TOTAL"; 
	private String ultimaLinhaSomaPrestacoes= null;
	private String ultimaLinhaSomaJuros = null;
	private String ultimaLinhaSomaAmortizacoes = null;
	private String ultimaLinhaBranca = "";
	//private String linhaTotal = "TOTAL";

	public void gerarPdf(List<Parcela> parcelas, int qtparcelas, int tipo) {
		if(tipo == 2) this.cabecalho = "SAC";
		if(tipo == 3) this.cabecalho = "PRICE";
		if(tipo == 4) this.cabecalho = "Americano";
		this.numeroLinhas = qtparcelas+1;
		
		for (Parcela p : parcelas) {
			
			if (p.getNumeroPrestacao() == qtparcelas+1){
				this.coluna1.add(String.valueOf(p.getTitulo()));
				this.ultimaLinhaSomaJuros = String.valueOf(p.getTotalJuros());
				this.ultimaLinhaSomaPrestacoes = String.valueOf(p.getTotalPrestacao());
				this.ultimaLinhaSomaAmortizacoes = String.valueOf(p.getTotalAmortizacao());
			}
			
			this.coluna1.add(String.valueOf(p.getNumeroPrestacao()));
			this.coluna2.add(String.format("%.2f", p.getPrestacao()));
			this.coluna3.add(String.format("%.2f", p.getValorJuros()));
			this.coluna4.add(String.format("%.2f", p.getAmortizacao()));
			this.coluna5.add(String.format("%.2f", p.getSaldoDevedor()));			
			
		}
		exportar();

	}

	private void exportar(){

		try {
			//cria o documento tamanho A4, margens de 2,54cm
			doc = new Document(PageSize.A4, 72, 72, 72, 72);

			//cria a stream de sa�da
			os = new FileOutputStream("C:/Users/MARCIO/Google Drive/Apoo/trabalhoamortiz/amortizacao"+cabecalho+(plus++)+".pdf");

			//associa a stream de sa�da ao 
			PdfWriter.getInstance(doc, os);

			//abre o documento
			doc.open();

			//adiciona o texto ao PDF
			Paragraph p = new Paragraph(this.titulo);
			doc.add(p);
			Paragraph p1 = new Paragraph(" ");
			doc.add(p1);
			PdfPTable table = new PdfPTable(colunas);

			PdfPCell header = new PdfPCell(new Paragraph(cabecalho));
			//o cabecalho ocupar� a largura da tabela
			header.setColspan(colunas);
			table.addCell(header);
			
			table.addCell(" ");
			table.addCell("Presta��o (R$)");
			table.addCell("Juros (R$)");
			table.addCell("Amortiza��o (R$) ");
			table.addCell("Saldo devedor  (R$)"); 
			
			//adiciona o corpo da tabela
			for(int i = 0; i<numeroLinhas; i++){
				table.addCell(coluna1.get(i));
				table.addCell(coluna2.get(i));
				table.addCell(coluna3.get(i));
				table.addCell(coluna4.get(i));
				table.addCell(coluna5.get(i));            	
				System.out.println("adicionando celula");
			}

			//adiciona a ultima linha da tabela
			
			//table.addCell(this.ultimaLinhaTotal);
			table.addCell(this.ultimaLinhaSomaPrestacoes);
			table.addCell(this.ultimaLinhaSomaJuros);
			table.addCell(this.ultimaLinhaSomaAmortizacoes);
			table.addCell(this.ultimaLinhaBranca);

			doc.add(table);
			System.out.println("adicionando tabela");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}finally{
			if(doc != null) doc.close();
			if(os != null)
				try {
					os.close();
					System.out.println("fechando pdf");
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

	}


	public ToPdf() {
		super();
		this.coluna1 = new ArrayList<String>();
		this.coluna2 = new ArrayList<String>();
		this.coluna3 = new ArrayList<String>();
		this.coluna4 = new ArrayList<String>();
		this.coluna5 = new ArrayList<String>();
		
	}



}
