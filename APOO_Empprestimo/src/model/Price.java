package model;

import java.util.ArrayList;
import java.util.List;

public class Price {
	private double valor;
	private double jurosTotal;
	private double totalAmortizacao;
	private double totalPrestacao;
	
	public List<Parcela> calculaPrice(double montante, int qtParcelas, double juros) {
		double valor = montante;
		double porc = juros/100;
		List<Parcela> parcelas = new ArrayList<Parcela>(); 


		for(int i = 1; i<=qtParcelas+1; i++){ 
			Parcela p = new Parcela(); 
			
			//calcula prestacao 
			p.setPrestacao(	montante*(((Math.pow(1+porc, qtParcelas))*porc)/ ((Math.pow(1+porc, qtParcelas))-1)));
			
			p.setAmortizacao(p.getPrestacao()-(porc*valor)); 
			
							
			p.setSaldoDevedor(valor - p.getAmortizacao());				
					
				
			
			
			//p.setValorJuros(valor*(1.5/100));
			//p.setPrestacao(valor/1-(1/Math.pow((1+juros), qtParcelas)));
			
			
			
			p.setNumeroPrestacao(i); 
			p.setValorJuros(porc * p.getSaldoDevedor() ); 

			 


			parcelas.add(p); 


			valor -=p.getAmortizacao(); 

			jurosTotal +=p.getValorJuros();
			totalAmortizacao+=p.getAmortizacao();
			totalPrestacao +=p.getPrestacao();

			if(i==qtParcelas+1){
				p.setTitulo("TOTAL");
				p.setValorJuros(jurosTotal);
				p.setValorPrestacao(totalPrestacao-p.getAmortizacao());
				p.setAmortizacao(totalAmortizacao-p.getAmortizacao());
				p.setSaldoDevedor(0);

			}
		} 


		return parcelas;
	}
	
	
}
