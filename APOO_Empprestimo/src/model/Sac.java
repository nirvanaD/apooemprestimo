package model;

import java.util.ArrayList;
import java.util.List;

public class Sac {
	private double valor;
	private double jurosTotal;
	private double totalAmortizacao;
	private double totalPrestacao;

	public List<Parcela> calculaSac(double montante, int qtParcelas, double juros) { 
		double valor = montante;
		
		List<Parcela> parcelas = new ArrayList<Parcela>(); 
		
		
		for(int i = 1; i<=qtParcelas+1; i++){ 
			Parcela p = new Parcela(); 
			
			p.setAmortizacao(montante/qtParcelas); 
			p.setSaldoAtual(valor); 
			p.setSaldoDevedor(valor - p.getAmortizacao());
			p.setNumeroPrestacao(i); 
			p.setValorJuros((juros/100) * p.getSaldoAtual() ); 
			
			p.setPrestacao(p.getValorJuros() + p.getAmortizacao()); 
			
			
			parcelas.add(p); 
			
			
			valor -=p.getAmortizacao(); 
			
			jurosTotal +=p.getValorJuros();
			totalAmortizacao+=p.getAmortizacao();
			totalPrestacao +=p.getPrestacao();
			
			if(i==qtParcelas+1){
				p.setTitulo("TOTAL");
				p.setValorJuros(jurosTotal);
				p.setValorPrestacao(totalPrestacao-p.getAmortizacao());
				p.setAmortizacao(totalAmortizacao-p.getAmortizacao());
				p.setSaldoDevedor(0);
				
			}
		} 
		
		
		return parcelas; 
	}
}


