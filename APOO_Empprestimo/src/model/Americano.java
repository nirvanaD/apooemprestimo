package model;
import java.util.ArrayList;
import java.util.List;

public class Americano {
	private double valor;
	private double jurosTotal;
	private double totalAmortizacao;
	private double totalPrestacao;

	public List<Parcela> calculaAmericano(double montante, int qtParcelas,
			double juros) {
		List<Parcela> parcelas = new ArrayList<Parcela>();

		for(int i = 1; i<=qtParcelas+1; i++){
			Parcela p = new Parcela();
			p.setNumeroPrestacao(i);
			if(i==0){
				p.setPrestacao(0);
				p.setAmortizacao(0);
				p.setSaldoDevedor(montante);
				p.setValorJuros(0);
				
			}else if(i == qtParcelas){
				p.setAmortizacao(montante);
				p.setSaldoDevedor(0);
				p.setValorJuros((juros/100) * montante);
				p.setPrestacao(montante+p.getValorJuros());

			}else{
				p.setAmortizacao(0);
				p.setValorJuros((juros/100) * montante);
				p.setPrestacao(p.getValorJuros());
				p.setSaldoDevedor(montante);
			}
			
			p.setNumeroPrestacao(i);
			parcelas.add(p);
			
			
			jurosTotal +=p.getValorJuros();
			totalAmortizacao+=p.getAmortizacao();
			totalPrestacao +=p.getPrestacao();
			
			if(i==qtParcelas+1){
				p.setTitulo("TOTAL");
				p.setValorJuros(jurosTotal);
				p.setValorPrestacao(totalPrestacao-p.getAmortizacao());
				p.setAmortizacao(totalAmortizacao-p.getAmortizacao());
				p.setSaldoDevedor(0);
				
			}
		}
		return parcelas;
	}
		
		
}
