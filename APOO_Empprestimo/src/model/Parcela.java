package model;

public class Parcela {
	private double valorPrestacao =0;
	private int numeroPrestacao = 0;
	private double valorJuros = 0;
	private double amortizacao = 0;
	private double saldoDevedor = 1;
	private double saldoAtual;
	private double totalJuros =0;
	private double totalAmortizacao;
	private double totalPrestacao;
	private String titulo;
	private double totalDevedor;
	
	public Parcela() {
		super();
	}
	
	public double getPrestacao() {
		return valorPrestacao;
	}
	public void setPrestacao(double prestacao) {
		this.valorPrestacao = prestacao;
	}
	public double getValorJuros() {
		return valorJuros;
	}
	public void setValorJuros(double valorJuros) {
		this.valorJuros = valorJuros;
	}
	
	public double getValorPrestacao() {
		return valorPrestacao;
	}
	public void setValorPrestacao(double valorPrestacao) {
		this.valorPrestacao = valorPrestacao;
	}
	public int getNumeroPrestacao() {
		return numeroPrestacao;
	}
	public void setNumeroPrestacao(int numeroPrestacao) {
		this.numeroPrestacao = numeroPrestacao;
	}
	public double getAmortizacao() {
		return amortizacao;
	}
	public void setAmortizacao(double amortizacao) {
		this.amortizacao = amortizacao;
	}
	public double getSaldoDevedor() {
		return saldoDevedor;
	}
	public void setSaldoDevedor(double saldoDevedor) {
		this.saldoDevedor = saldoDevedor;
	}

	public double getSaldoAtual() {
		return saldoAtual;
	}

	public void setSaldoAtual(double sadoAtual) {
		this.saldoAtual = sadoAtual;
	}

	public double getTotalJuros() {
		return totalJuros;
	}

	public void setTotalJuros(double totalJuros) {
		this.totalJuros = totalJuros;
	}

	public double getTotalAmortizacao() {
		return totalAmortizacao;
	}

	public void setTotalAmortizacao(double totalAmortizacao) {
		this.totalAmortizacao = totalAmortizacao;
	}

	public double getTotalPrestacao() {
		return totalPrestacao;
	}

	public void setTotalPrestacao(double totalPrestacao) {
		this.totalPrestacao = totalPrestacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getTotalDevedor() {
		return totalDevedor;
	}

	public void setTotalDevedor(double totalDevedor) {
		this.totalDevedor = totalDevedor;
	}

	

	

}
