package model;

import static org.junit.Assert.*;

import java.util.List;

import model.Americano;
import model.Parcela;

import org.junit.Test;

public class testAmericano {
	
	@Test
	public void testaAmericano(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 1);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 0 )
			assertEquals(0, parcela.getAmortizacao(), 0.1);
		}
	}	
	
	@Test
	public void testaAmericano6(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 1);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 1 )
			assertEquals(1, parcela.getValorPrestacao(), 0.1);
		}
	}
	
	
	@Test
	public void testaAmericano2(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 1);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 1 || parcela.getNumeroPrestacao() == 2 )
			assertEquals(0, parcela.getAmortizacao(), 0.1);
		}
	}
	/*
	
	
	@Test
	public void testaAmericano3(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 1);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 3 )
			assertEquals(100, parcela.getAmortizacao(), 0.1);
		}
	}
	
	@Test
	public void testaAmericano4(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 1);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 3 )
			assertEquals(0, parcela.getSaldoDevedor(), 0.1);
		}
	}
	
	@Test
	public void testaAmericano5(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 1);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() != 3 )
			assertEquals(100, parcela.getSaldoDevedor(), 0.1);
		}
	}
	
	
	
	
	@Test
	public void testaAmericano7(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 5);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 2 )
			assertEquals(5, parcela.getValorPrestacao(), 0.1);
		}
	}
	
	@Test
	public void testaAmericano8(){
		Americano a = new  Americano();
		List<Parcela>  p = a.calculaAmericano(100, 3, 5);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() == 3 )
			assertEquals(105, parcela.getValorPrestacao(), 0.01);
		}
	}
	*/
}
