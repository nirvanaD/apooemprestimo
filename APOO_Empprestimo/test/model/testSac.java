package model;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;


public class testSac {
		
	@Test
	public void testSac1(){
		Sac c = new Sac();
		List<Parcela> p = c.calculaSac(120000, 10, 5);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() <= 5)
			assertEquals(12000, parcela.getAmortizacao(), 0.1);
		}
	}	
	
	
}
