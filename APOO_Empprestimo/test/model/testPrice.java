package model;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class testPrice {
	@Test
	public void testPrice1(){
		Price c = new Price();
		List<Parcela> p = c.calculaPrice(20000, 4, 8);
		for (Parcela parcela : p) {
			if(parcela.getNumeroPrestacao() >=8)
				assertEquals(2970.56, parcela.getPrestacao(), 0.1);
			
		}
	}	
}
